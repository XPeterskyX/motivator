package pl.lodz.p.se.motivator;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pl.lodz.p.se.database.DatabaseMock;
import pl.lodz.p.se.motivator.api.MotivatorAPI;

public class MainActivity extends AppCompatActivity {

    MotivatorMain motivatorMain;

    DatabaseMock databaseMock;

    JSONArray members;

    MotivatorAPI motivatorAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_main);

        final EditText name = (EditText) findViewById(R.id.name);
        final Button register = (Button) findViewById(R.id.register);
        final Button refresh = (Button) findViewById(R.id.refresh);
        final Spinner friendSpinner = (Spinner) findViewById(R.id.friendSpinner);
        final Button levelFinished = (Button) findViewById(R.id.levelFinished);
        final Button levelStarted = (Button) findViewById(R.id.levelStarted);
        final Button achievement = (Button) findViewById(R.id.achievement);

        final String[][] friendArray = {{}};

        // NotRegistered textView
        final TextView notRegistered = (TextView) findViewById(R.id.notRegistered);

        databaseMock = new DatabaseMock();

        // Name editText
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name.setText("");
                register.setEnabled(true);
            }
        });

        // Register button
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                motivatorMain.configFile.readConfigFile();
                if (name.getText().toString().isEmpty()) {
                    name.setText("Enter your name");
                    return;
                } else {
                    motivatorMain.configFile.setName(name.getText().toString());
                    motivatorMain.configFile.getFileContent();
                    JSONObject response = databaseMock.registerUser(motivatorMain.configFile.getFileJSON());
                    int id = -1;
                    try {
                        id = response.getInt("id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (id != -1) {
                        try {
                            response.put("registered", true);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        motivatorMain.configFile.setFileJSON(response);
                        notRegistered.setText("Registered as " + motivatorMain.configFile.getName());
                    }
                }
            }
        });
        register.setEnabled(false);

        motivatorMain = new MotivatorMain(this);
        motivatorMain.onStartup();

        if (motivatorMain.configFile.isRegistered()) {
            notRegistered.setText("Registered as " + motivatorMain.configFile.getName());
        }

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                members = databaseMock.getAllMembers();
                System.out.println(members.toString());
                friendArray[0] = new String[members.length()];
                for (int i = 0; i < members.length(); i++) {
                    try {
                        JSONObject member = members.getJSONObject(i);
                        friendArray[0][i] = member.getString("name");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                ArrayAdapter adapter = new ArrayAdapter(getApplicationContext(),
                        android.R.layout.simple_spinner_item, friendArray[0]);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                friendSpinner.setAdapter(adapter);
            }
        });

        levelStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject friend = new JSONObject();
                System.out.println("Tu jeszcze OK");
                System.out.println(friendSpinner.getSelectedItem().toString());
                for (int i = 0; i < members.length(); i++) {
                    try {
                        friend = members.getJSONObject(i);
                        if (friend.getString("name").equals(friendSpinner.getSelectedItem().toString())) {
                            System.out.println("Notification set to: " + friend);
                            try {
                                System.out.println("Result: " + motivatorAPI.onLevelStart(friend, "new level"));
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        levelFinished.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject friend = new JSONObject();
                System.out.println("Tu jeszcze OK");
                System.out.println(friendSpinner.getSelectedItem().toString());
                for (int i = 0; i < members.length(); i++) {
                    try {
                        friend = members.getJSONObject(i);
                        if (friend.getString("name").equals(friendSpinner.getSelectedItem().toString())) {
                            System.out.println("Notification set to: " + friend);
                            try {
                                System.out.println("Result: " + motivatorAPI.onLevelFinished(friend, "last level"));
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        achievement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject friend = new JSONObject();
                System.out.println("Tu jeszcze OK");
                System.out.println(friendSpinner.getSelectedItem().toString());
                for (int i = 0; i < members.length(); i++) {
                    try {
                        friend = members.getJSONObject(i);
                        if (friend.getString("name").equals(friendSpinner.getSelectedItem().toString())) {
                            System.out.println("Notification set to: " + friend);
                            try {
                                System.out.println("Result: " + motivatorAPI.onAchievement(friend, "Tutorial"));
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        motivatorAPI = new MotivatorAPI();
    }

    @Override
    protected void onStop() {
        super.onStop();
        motivatorMain.onStop();
    }
}
