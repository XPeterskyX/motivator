package pl.lodz.p.se.motivator.api;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by piotr on 09.12.15.
 */
public class MotivatorAPI {

    String apiURL = "http://ujkk93e988da.januszprogramowania.koding.io:4567/notify";

    public boolean onLevelStart(JSONObject friend, String levelName){
        boolean result = false;

        JSONObject message = new JSONObject();
        try {
            message.put("to",friend.getString("token"));
            JSONObject obj = new JSONObject("{\"message\":\"Your friend "+friend.getString("name")+" has started "+levelName+"\"}");
            message.put("data",obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            URL obj = new URL(apiURL);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            System.out.println("This will be sent: "+message);

            // For POST only - START
            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            os.write(message.toString().getBytes());
            os.flush();
            os.close();
            // For POST only - END

            int responseCode = con.getResponseCode();
            System.out.println("POST Response Code :: " + responseCode);
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer resp = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                resp.append(inputLine);
            }
            in.close();

            System.out.println("Response from members POST: " + resp);
            JSONObject response = new JSONObject(resp.toString());
            if(response.getInt("success")==1){
                result = true;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean onLevelFinished(JSONObject friend, String levelName){
        boolean result = false;

        JSONObject message = new JSONObject();
        try {
            message.put("to",friend.getString("token"));
            JSONObject obj = new JSONObject("{\"message\":\"Your friend "+friend.getString("name")+" has finished "+levelName+"\"}");
            message.put("data",obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            URL obj = new URL(apiURL);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            // For POST only - START
            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            os.write(message.toString().getBytes());
            os.flush();
            os.close();
            // For POST only - END

            int responseCode = con.getResponseCode();
            System.out.println("POST Response Code :: " + responseCode);
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer resp = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                resp.append(inputLine);
            }
            in.close();

            System.out.println("Response from members POST: " + resp);
            JSONObject response = new JSONObject(resp.toString());
            if(response.getInt("success")==1){
                result = true;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean onAchievement(JSONObject friend, String achievementName){
        boolean result = false;

        JSONObject message = new JSONObject();
        try {
            message.put("to",friend.getString("token"));
            JSONObject obj = new JSONObject("{\"message\":\"Your friend "+friend.getString("name")+" finished "+achievementName+"\"}");
            message.put("data",obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            URL obj = new URL(apiURL);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            // For POST only - START
            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            os.write(message.toString().getBytes());
            os.flush();
            os.close();
            // For POST only - END

            int responseCode = con.getResponseCode();
            System.out.println("POST Response Code :: " + responseCode);
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer resp = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                resp.append(inputLine);
            }
            in.close();

            System.out.println("Response from members POST: " + resp);
            JSONObject response = new JSONObject(resp.toString());
            if(response.getInt("success")==1){
                result = true;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }
}
