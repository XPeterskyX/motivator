package pl.lodz.p.se.motivator.config;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by piotr on 07.12.15.
 */
public class ConfigFile {
    private Context ctx;
    private String configFileName = "config.json";

    private String fileContent;
    private JSONObject fileJSON;

    private String name;
    private boolean registered;
    private String token;
    private String id;

    public ConfigFile(Context ctx) {
        this.ctx = ctx;

        fileJSON = new JSONObject();

        boolean configFlag = false;
        String[] fileList = ctx.fileList();
        for (String fileName : fileList) {
            if (fileName.equals(configFileName)) {
                configFlag = true;
                break;
            }
        }
        if (configFlag) {
            System.out.println("Found config file");
            readConfigFile();
            try {
                this.registered = fileJSON.getBoolean("registered");
                this.name = fileJSON.getString("name");
                this.token = fileJSON.getString("token");
                this.id = fileJSON.getString("id");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Config file not found");
            name = "";
            registered = false;
            token = "";
            id = "";
            saveConfigFile();
        }
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRegistered() {

        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    public JSONObject getFileJSON() {
        return fileJSON;
    }

    public void setFileJSON(JSONObject fileJSON) {
        this.fileJSON = fileJSON;
        try {
            this.registered = fileJSON.getBoolean("registered");
            this.name = fileJSON.getString("name");
            this.token = fileJSON.getString("token");
            this.id = fileJSON.getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getToken() {

        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFileContent() {
        try {
            fileJSON.put("registered", this.registered);
            fileJSON.put("name", this.name);
            fileJSON.put("id", this.id);
            fileJSON.put("token", this.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.fileContent = fileJSON.toString();
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    public void readConfigFile() {
        try (FileInputStream fis = ctx.openFileInput(configFileName);) {
            int content;
            while ((content = fis.read()) != -1) {
                fileContent += (char) content;
            }
            // Weird bug here
            fileContent = fileContent.substring(4);
            System.out.println("File content: " + fileContent);
            fileJSON = new JSONObject(fileContent);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveConfigFile() {
        getFileContent();
        System.out.println("This will be saved to a file: " + fileContent);
        try (FileOutputStream fos = ctx.openFileOutput(configFileName, Context.MODE_PRIVATE)) {
            fos.write(fileContent.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void save(){
        getFileContent();
        saveConfigFile();
    }
}
