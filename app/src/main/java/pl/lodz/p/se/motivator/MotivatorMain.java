package pl.lodz.p.se.motivator;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import pl.lodz.p.se.motivator.config.ConfigFile;
import pl.lodz.p.se.motivator.notifier.QuickstartPreferences;
import pl.lodz.p.se.motivator.notifier.RegistrationIntentService;

/**
 * Created by piotr on 08.12.15.
 */
public class MotivatorMain {
    Context ctx;

    ConfigFile configFile;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MotivatorMain";

    public MotivatorMain(Context ctx) {
        this.ctx = ctx;
    }

    public void onStartup(){
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    System.out.println(ctx.getString(R.string.gcm_send_message));
                } else {
                    System.out.println(ctx.getString(R.string.token_error_message));
                }
            }
        };

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(ctx, RegistrationIntentService.class);
            ctx.startService(intent);
        }

        configFile = new ConfigFile(ctx);
    }

    public void onStop(){
        configFile.save();
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(ctx);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog((Activity) ctx, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }
}
